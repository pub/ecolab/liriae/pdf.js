const SPACY_COLORS = {
  LOC: [255, 68, 51],
  JUR: [233, 30, 99],
  DIM: [156, 39, 176],
  ORG: [63, 81, 181],
  PER: [33, 150, 243],
  MISC: [0, 188, 212],
  EAU: [76, 175, 80],
  FF: [205, 220, 57],
  MP: [255, 152, 0],
  POLL: [121, 85, 72],
  TRA: [158, 158, 158],
};

class StickerSelector {
  #stickersData;

  constructor(options, overlayManager, eventBus) {
    this.dialog = options.dialog;
    this.closeButton = options.closeButton;
    this.stickerContainer = options.stickerContainer;
    this.overlayManager = overlayManager;
    this.eventBus = eventBus;
    this.stickersStatus = {};

    // Attach the event listeners.
    this.eventBus._on("openstickerswindow", this.open.bind(this));
    this.closeButton.addEventListener("click", this.close.bind(this));

    this.overlayManager.register(this.dialog, /* canForceClose = */ true);
  }

  async open() {
    await this.overlayManager.open(this.dialog);

    if (!this.#stickersData) {
      // Fetch the stickers data
      this.#stickersData = await this.#getStickersData();

      // Build the window
      const inputNode = document.createElement("input");
      inputNode.type = "checkbox";
      const labelNode = document.createElement("label");
      this.#stickersData.forEach(stickerData => {
        const color = SPACY_COLORS[stickerData.spacy_label];
        const rgb = `rgba(${color.join()},0.3)`;
        this.stickersStatus[stickerData.spacy_label] = {
          status: false,
          color: rgb,
        };

        // Build nodes
        const container = document.createElement("div");
        const inputStickerNode = inputNode.cloneNode();
        const labelStickerNode = labelNode.cloneNode();

        inputStickerNode.dataset.spacyLabel = stickerData.spacy_label;
        inputStickerNode.dataset.userLabel = stickerData.user_label;
        labelStickerNode.style.color = `rgb(${color.join()})`;
        labelStickerNode.style.fontWeight = "bold";

        const name = `stickerselector${stickerData.spacy_label}`;
        inputStickerNode.name = name;
        inputStickerNode.id = name;
        labelStickerNode.htmlFor = name;

        labelStickerNode.textContent = stickerData.user_label;

        // Bind Events
        inputStickerNode.addEventListener("change", event => {
          this.stickersStatus[stickerData.spacy_label].status =
            event.currentTarget.checked;
          this.eventBus.dispatch("stickersupdated", {
            status: this.stickersStatus,
          });
        });

        // Add Nodes
        container.append(inputStickerNode);
        container.append(labelStickerNode);
        this.stickerContainer.append(container);
      });
    }

    // this.label.textContent = await this.l10n.get(
    // `password_${passwordIncorrect ? "invalid" : "label"}`
    // );
  }

  async close() {
    if (this.overlayManager.active === this.dialog) {
      this.overlayManager.close(this.dialog);
    }
  }

  async #getStickersData() {
    // TODO Make it retry on error
    const response = await fetch(
      "https://user-blenzi-147818-user.user.lab.sspcloud.fr/annotations/entity_labels"
    );
    const stickersData = await response.json();
    return stickersData;
  }
}

export { StickerSelector };
