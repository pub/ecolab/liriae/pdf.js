import { getDocId } from "./liriae_utils.js";

class StickerLayer {
  #pagesRendered = [];

  #pageWorkers = {};

  #stickerCanvas = {};

  #stickersStatus;

  constructor(app) {
    this.app = app;
    this.eventBus = app.eventBus;

    this.eventBus._on("pagerendered", this.pageRendered.bind(this));
    this.eventBus._on("stickersupdated", this.stickersUpdated.bind(this));
  }

  pageRendered({ source, pageNumber }) {
    this.#pagesRendered.push(source);
    this.updateStickers(source);
  }

  stickersUpdated({ status }) {
    this.#stickersStatus = status;
    this.updateAllStickers();
  }

  async drawStickers(page, pageNumber) {
    if (!this.#stickersStatus || !!this.#pageWorkers[pageNumber]) {
      return;
    }

    this.#pageWorkers[pageNumber] = true;

    const stickers = await this.getPageStickers(
      pageNumber,
      this.#stickersStatus
    );
    const canvas = this.#stickerCanvas[page.id];
    canvas.cleared = false;
    const ctx = canvas.getContext("2d");

    for (const sticker of stickers) {
      ctx.fillStyle = this.#stickersStatus[sticker.spacy_label].color;

      sticker.bbox = page.viewport.convertToViewportRectangle(sticker.bbox);
      const x = sticker.bbox[0];
      const height = sticker.bbox[3] - sticker.bbox[1];
      // For some reason, we get the wrong y and need to fix it
      // see https://github.com/mozilla/pdf.js/issues/5643#issuecomment-239993212
      const y = canvas.height - (sticker.bbox[1] + height);
      const width = sticker.bbox[2] - sticker.bbox[0];
      ctx.fillRect(x, y, width, height);
    }
    this.#pageWorkers[pageNumber] = false;
  }

  updateStickers(pageView) {
    if (!pageView || !pageView.canvas) {
      return;
    } // Page load has been aborted

    const canvasWrapper = pageView.canvas.parentNode;

    let stickerCanvas = this.#stickerCanvas[pageView.id];

    if (!!stickerCanvas && stickerCanvas.height !== pageView.canvas.height) {
      stickerCanvas = stickerCanvas.remove();
    } // Also set to undefined

    if (!stickerCanvas || !stickerCanvas.isConnected) {
      // It may not exist or have been disconnected from the DOM
      canvasWrapper.style.position = "relative";
      pageView.canvas.style.position = "absolute";
      if (!stickerCanvas) {
        stickerCanvas = pageView.canvas.cloneNode();
        this.#stickerCanvas[pageView.id] = stickerCanvas;
      } else {
        stickerCanvas.remove();
      }
      canvasWrapper.append(stickerCanvas);
    }

    if (!stickerCanvas.cleared) {
      stickerCanvas
        .getContext("2d")
        .clearRect(0, 0, stickerCanvas.height, stickerCanvas.height);
      stickerCanvas.cleared = true;
    }

    if (!pageView.canvas) {
      return;
    }

    this.drawStickers(pageView, pageView.id);
  }

  updateAllStickers() {
    for (const pageView of this.#pagesRendered) {
      this.updateStickers(pageView);
    }
  }

  async getPageStickers(pageNumber, stickerStatus) {
    const pageIndex = pageNumber - 1;
    let stickers;

    try {
      const labels = Object.keys(this.#stickersStatus)
        .filter(label => this.#stickersStatus[label].status)
        .map(label => `&type=${label}`)
        .join("");

      const id = getDocId(this.app.url);
      const response = await fetch(
        `https://liriae-back-dev.lab.sspcloud.fr/api/annotations?docId=${id}&page=${pageIndex}${labels}`
      );
      const data = await response.json();
      stickers = data.results;
    } catch (ex) {
      console.error(ex);
      return [];
    }

    // TODO use stickerStatus to limit amount of data over the wire
    // Needs backend to be up with it
    // In the meantime, we'll filter instead
    stickers = stickers.filter(
      sticker => stickerStatus[sticker.spacy_label].status
    );
    return stickers;
  }
}

export { StickerLayer };
