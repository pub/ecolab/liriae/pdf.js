const UUIDRegexp = new RegExp(
  "^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$"
);

function getDocId(url) {
  // Used for testing purposes
  const idsInUrl = [...url.matchAll(/(\d+)/g)];
  // Used in production
  const urlParts = url.split("/");
  const lastURLPart = urlParts.at(-1);
  const id = UUIDRegexp.test(lastURLPart) ? lastURLPart : idsInUrl.at(-1)[0];
  return id;
}

export { getDocId };
