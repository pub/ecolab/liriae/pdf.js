import { getDocId } from "./liriae_utils.js";

class FindBarEnhancedLayer {
  #pagesRendered = [];

  #pageWorkers = {};

  #searchCanvas = {};

  #searchData;

  constructor(app) {
    this.app = app;
    this.eventBus = app.eventBus;

    this.eventBus._on("pagerendered", this.pageRendered.bind(this));
    this.eventBus._on("findenhanced", this.searchDataUpdated.bind(this));
  }

  pageRendered({ source, pageNumber }) {
    this.#pagesRendered.push(source);
    this.updateStickers(source);
  }

  searchDataUpdated({ searchData }) {
    this.#searchData = searchData;
    this.updateAllStickers();
  }

  async drawStickers(page, pageNumber) {
    if (!this.#searchData || !!this.#pageWorkers[pageNumber]) {
      return;
    }

    this.#pageWorkers[pageNumber] = true;

    const stickers = await this.getPageSearchResults(
      pageNumber,
      this.#searchData
    );

    if (!stickers) {
      return;
    }

    const canvas = this.#searchCanvas[page.id];
    canvas.cleared = false;
    const ctx = canvas.getContext("2d");

    for (const sticker of stickers) {
      ctx.fillStyle = "rgba(0, 0, 255, 0.3)";

      sticker.bbox = page.viewport.convertToViewportRectangle(sticker.bbox);
      const x = sticker.bbox[0];
      const height = sticker.bbox[3] - sticker.bbox[1];
      // For some reason, we get the wrong y and need to fix it
      // see https://github.com/mozilla/pdf.js/issues/5643#issuecomment-239993212
      const y = canvas.height - (sticker.bbox[1] + height);
      const width = sticker.bbox[2] - sticker.bbox[0];
      ctx.fillRect(x, y, width, height);
    }
    this.#pageWorkers[pageNumber] = false;
  }

  updateStickers(pageView) {
    if (!pageView || !pageView.canvas) {
      return;
    } // Page load has been aborted

    const canvasWrapper = pageView.canvas.parentNode;

    let searchCanvas = this.#searchCanvas[pageView.id];

    if (!!searchCanvas && searchCanvas.height !== pageView.canvas.height) {
      searchCanvas = searchCanvas.remove();
    } // Also set to undefined

    if (!searchCanvas || !searchCanvas.isConnected) {
      // It may not exist or have been disconnected from the DOM
      canvasWrapper.style.position = "relative";
      pageView.canvas.style.position = "absolute";
      if (!searchCanvas) {
        searchCanvas = pageView.canvas.cloneNode();
        this.#searchCanvas[pageView.id] = searchCanvas;
      } else {
        searchCanvas.remove();
      }
      canvasWrapper.append(searchCanvas);
    }

    if (!searchCanvas.cleared) {
      searchCanvas
        .getContext("2d")
        .clearRect(0, 0, searchCanvas.height, searchCanvas.height);
      searchCanvas.cleared = true;
    }

    if (!pageView.canvas) {
      return;
    }

    this.drawStickers(pageView, pageView.id);
  }

  updateAllStickers() {
    for (const pageView of this.#pagesRendered) {
      this.updateStickers(pageView);
    }
  }

  async getPageSearchResults(pageNumber, searchData) {
    const pageIndex = pageNumber - 1;
    let stickers;

    try {
      const id = getDocId(this.app.url);
      const response = await fetch(
        `https://liriae-pdf-dev.lab.sspcloud.fr/search/words/${id}?query=${searchData}&page=${pageIndex}`
      );
      const data = await response.json();
      stickers = data.words ?? [];
    } catch (ex) {
      console.error(ex);
      return [];
    }

    stickers = stickers.map(wordData => wordData._source);
    return stickers;
  }
}

export { FindBarEnhancedLayer };
