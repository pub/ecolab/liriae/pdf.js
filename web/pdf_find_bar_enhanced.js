import { getDocId } from "./liriae_utils.js";

class PDFFindBarEnhanced {
  constructor(app, options, l10n) {
    const inputWaitTime = 3000;

    this.app = app;
    this.opened = false;
    this._inputWaitTime = inputWaitTime;

    this.bar = options.bar;
    this.toggleButton = options.toggleButton;
    this.findField = options.findField;
    this.eventBus = app.eventBus;
    this.timer = null;

    // Add event listeners to the DOM elements.
    this.toggleButton.addEventListener("click", () => {
      this.toggle();
    });

    this.findField.addEventListener("input", () => {
      window.clearTimeout(this.timer);
      this.timer = window.setTimeout(
        () => this.dispatchEventWithEnchancedSearchData(),
        inputWaitTime
      );
    });

    this.bar.addEventListener("keydown", e => {
      switch (e.keyCode) {
        case 13: // Enter
          if (e.target === this.findField) {
            window.clearTimeout(this.timer);
            // this.dispatchEventWithEnchancedSearchData();
            this.dispatchEvent(this.findField.value);
          }
          break;
        case 27: // Escape
          this.close();
          break;
      }
    });

    this.eventBus._on("resize", this.#adjustWidth.bind(this));
  }

  // reset() {
  // this.updateUIState();
  // }

  async #getEnhancedSearchData() {
    // Make the API calls
    const docId = getDocId(this.app.url);
    const response = await fetch(
      `https://liriae-back-dev.lab.sspcloud.fr/api/content/search?q=${this.findField.value}&docId=${docId}`
    );
    const searchData = await response.json();
    return searchData;
  }

  async dispatchEventWithEnchancedSearchData() {
    this.#getEnhancedSearchData().then(searchData => {
      this.dispatchEvent(searchData);
    });
  }

  dispatchEvent(searchData) {
    this.eventBus.dispatch("findenhanced", {
      source: this,
      searchData,
    });
  }

  // updateUIState(state, previous, matchesCount) {
  // let findMsg = Promise.resolve("");
  // let status = "";

  // switch (state) {
  // case FindState.FOUND:
  // break;
  // case FindState.PENDING:
  // status = "pending";
  // break;
  // case FindState.NOT_FOUND:
  // findMsg = this.l10n.get("find_not_found");
  // status = "notFound";
  // break;
  // case FindState.WRAPPED:
  // findMsg = this.l10n.get(`find_reached_${previous ? "top" : "bottom"}`);
  // break;
  // }
  // this.findField.setAttribute("data-status", status);
  // this.findField.setAttribute("aria-invalid", state === FindState.NOT_FOUND);

  // findMsg.then(msg => {
  // this.findMsg.textContent = msg;
  // this.#adjustWidth();
  // });

  // this.updateResultsCount(matchesCount);
  // }

  updateResultsCount({ current = 0, total = 0 } = {}) {
    // TODO
    // const limit = MATCHES_COUNT_LIMIT;
    // let matchCountMsg = Promise.resolve("");
    // if (total > 0) {
    // if (total > limit) {
    // let key = "find_match_count_limit";
    // if (typeof PDFJSDev !== "undefined" && PDFJSDev.test("MOZCENTRAL")) {
    // // TODO: Remove this hard-coded `[other]` form once plural support has
    // // been implemented in the mozilla-central specific `l10n.js` file.
    // key += "[other]";
    // }
    // matchCountMsg = this.l10n.get(key, { limit });
    // } else {
    // let key = "find_match_count";
    // if (typeof PDFJSDev !== "undefined" && PDFJSDev.test("MOZCENTRAL")) {
    // // TODO: Remove this hard-coded `[other]` form once plural support has
    // // been implemented in the mozilla-central specific `l10n.js` file.
    // key += "[other]";
    // }
    // matchCountMsg = this.l10n.get(key, { current, total });
    // }
    // }
    // matchCountMsg.then(msg => {
    // this.findResultsCount.textContent = msg;
    // // Since `updateResultsCount` may be called from `PDFFindController`,
    // // ensure that the width of the findbar is always updated correctly.
    // this.#adjustWidth();
    // });
  }

  open() {
    if (!this.opened) {
      this.opened = true;
      this.toggleButton.classList.add("toggled");
      this.toggleButton.setAttribute("aria-expanded", "true");
      this.bar.classList.remove("hidden");
    }
    this.findField.select();
    this.findField.focus();

    this.#adjustWidth();
  }

  close() {
    if (!this.opened) {
      return;
    }
    this.opened = false;
    this.toggleButton.classList.remove("toggled");
    this.toggleButton.setAttribute("aria-expanded", "false");
    this.bar.classList.add("hidden");

    this.eventBus.dispatch("findbarclose", { source: this });
  }

  toggle() {
    if (this.opened) {
      this.close();
    } else {
      this.open();
    }
  }

  #adjustWidth() {
    if (!this.opened) {
      return;
    }

    // The find bar has an absolute position and thus the browser extends
    // its width to the maximum possible width once the find bar does not fit
    // entirely within the window anymore (and its elements are automatically
    // wrapped). Here we detect and fix that.
    this.bar.classList.remove("wrapContainers");

    const findbarHeight = this.bar.clientHeight;
    const inputContainerHeight = this.bar.firstElementChild.clientHeight;

    if (findbarHeight > inputContainerHeight) {
      // The findbar is taller than the input container, which means that
      // the browser wrapped some of the elements. For a consistent look,
      // wrap all of them to adjust the width of the find bar.
      this.bar.classList.add("wrapContainers");
    }
  }
}

export { PDFFindBarEnhanced };
